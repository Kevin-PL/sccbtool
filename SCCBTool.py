#-*- coding: utf-8 -*-
'''
@File       : SCCBTool.py
@Date       : 2021/9/4
@Author     : Kevin Peng
@Version    : 0.1
@License    : MIT License
@Dependecy  : Python 3+
@Description: SCCB access tool (used for OmniVision sensor/chip access)
'''

import os, sys, re
import configparser
import json
import tkinter as tk
from tkinter import ttk
# from tkinter import simpledialog
from tkinter import Widget
import tkinter.messagebox

import lib.windnd as windndlib
import lib.dialog as dialoglib 
import lib.tooltip as tooltiplib
import lib.verticalscrollframe as vscrollframelib
from lib.setting import *

def drag_file(files):
    try:
        msg = '\n'.join([i.decode('utf-8') for i in files])
    except UnicodeDecodeError as err:
        msg = '\n'.join([i.decode('gbk') for i in files])
    tk.messagebox.showinfo("drag-in file", msg)

class Note:
    def __init__(self, root):
        self.root = root
        self._create_ui()

    def _create_ui(self):
        self.notebook = ttk.Notebook(self.root)
        self.frame1 = tk.Frame(self.root)
        self.frame2 = tk.Frame(self.root)
        self.label1 = tk.Label(self.frame1, text="python", bg='lightblue', fg='red')
        self.label1.pack(padx=200, pady=50, ipadx=100, ipady=100)
        self.button1 = tk.Button(self.frame2, text="press", command=self._button_cb)
        self.button1.pack(padx=200, pady=5)
        self.notebook.add(self.frame1, text="tab1")
        self.notebook.add(self.frame2, text="tab2")
        self.frame1.bind("<2>", self._tab_right_click)
        self.notebook.pack(padx=10, pady=5, fill=tk.BOTH, expand=True)
        
        windndlib.hook_dropfiles(self.label1, func=self._drag_cb)

    def _tab_right_click(self, events):
        self.notebook.forget(self.frame1)

    def _button_cb(self):
        print("hello world")
    
    def _drag_cb(self, files):
        try:
            msg = '\n'.join([i.decode('utf-8') for i in files])
        except UnicodeDecodeError as err:
            msg = '\n'.join([i.decode('gbk') for i in files])
        print(msg)

class SettingDialog(tk.Toplevel):
    '''setting config dialog'''
    def __init__(self, parent, title=None, settinglist=[]):
        super().__init__(parent)
        self.transient(parent)
        if title:
            self.title(title)   # set title
        self.parent = parent
        self.fr_dlg = tk.Frame(self)  # dialog basic frame
        self.initial_focus = self.body(self.fr_dlg)
        self.fr_dlg.pack(side='top', fill='both', expand=True, padx=5, pady=5)
        self._init_ui()
        self.grab_set()  # set block mode
        if not self.initial_focus:
            self.initial_focus = self
        # set destroy function
        self.protocol("WM_DELETE_WINDOW", self.cancel)  # set destroy
        # set original dialog size
        self.update()  # important, otherwise height/width is not correct
        self.geometry("+%d+%d" % (parent.winfo_x()+parent.winfo_width()//2-self.winfo_width()//2,
                                  parent.winfo_y()+parent.winfo_height()//2-self.winfo_height()//2))
        print(self.winfo_width(), parent.winfo_height())
        # attr
        self.result   = []  # return list
        self.initlist = settinglist  # original input setting list, to determine the setting format
        # set original status
        self.initial_focus.focus_set()
        self.wait_window(self)

    def body(self, master):
        # create dialog body.  return widget that should have
        # initial focus.  this method should be overridden
        pass

    def _init_ui(self):
        '''ui init function
        descp:
            add standard button box, override if you don't want the standard buttons
        args:
            none
        return:
            none
        '''
        ## resize dialog
        self.geometry("400x200")
        ## add treeview widget
        columns = ("ID", "ADDR", "VAL")
        self.trv = ttk.Treeview(self.fr_dlg, height=5, show="headings", columns=columns)
        self.trv.pack(side="top", fill='x')
        self.trv.heading("ID", text="ID")
        self.trv.heading("ADDR", text="ADDR")
        self.trv.heading("VAL", text="VAL")
        self.trv.column("ID", width=20, anchor="center")
        self.trv.column("ADDR", width=40, anchor="center")
        self.trv.column("VAL", width=40, anchor="center", stretch=True)
        # bind handler function
        self.trv.bind('<Double-1>', self._set_cell_value)
        # FOR TEST
        self.trv.insert(parent="", index="end", text="TEST", values=("48", "4000", "d1"))
        self.trv.insert(parent="", index="end", text="TEST", values=("48", "4000", "d1"))
        self.trv.insert(parent="", index="end", text="TEST", values=("48", "4000", "d1"))
        # treeview scroll bar
        self.trv_scroll = tk.Scrollbar(self.fr_dlg, orient="vertical", command=self.trv.yview)
        self.trv.configure(yscrollcommand=self.trv_scroll.set)
        # right click menu
        self.popup_menu = tk.Menu(self.trv, tearoff=0)
        self.trv.bind('<Button-3>', self._popup)
        self.popup_menu.add_command(label="test", command=self._test_menu)
        m = tk.Menu(self.popup_menu, tearoff=0)  # add sub menu
        self.popup_menu.add_cascade(label="for test", menu=m)
        ## add button
        btn_ok = tk.Button(self, text="OK", width=10, command=self.ok, default=tk.ACTIVE)
        btn_cancel = tk.Button(self, text="Cancel", width=10, command=self.cancel)
        # btn_ok.pack(side="left", padx=5, pady=5)
        # btn_cancel.pack(side="left", padx=5, pady=5)
        btn_cancel.pack(side="right", padx=5, pady=5)
        btn_ok.pack(side="right", padx=5, pady=5)
        ## bind 'enter' / 'esc' press event
        self.bind("<Return>", self.ok)
        self.bind("<Escape>", self.cancel)
    
    def _set_cell_value(self, event):
        '''set cell value of treeview'''
        if event.widget.selection():
            tree_idx = event.widget.selection()[0]
            col_idx = self.trv.identify_column(event.x)  # identify tree columm
            row_idx = self.trv.identify_row(event.y)  # identify tree row
            print(col_idx, row_idx)
            try:
                col_num = int(re.findall(r"\d+", col_idx)[0])
                row_num = int(re.findall(r"\d+", row_idx)[0])
            except Exception as err:
                print("Error: %s" % err)
            else:
                v = dialoglib.askstring("modify value", "input", parent=self.trv, position=(event.x_root, event.y_root), initialvalue=self.trv.item(tree_idx, "values")[col_num-1])
                self.trv.set(tree_idx, column=col_idx, value=v)

    def _popup(self, event):
        self.popup_menu.post(event.x_root, event.y_root)
    
    def _test_menu(self):
        print("success invoke.")

    def ok(self, event=None):
        '''press 'ok' task'''
        if not self.validate():
            self.initial_focus.focus_set()  # put focus back
            return
        self.withdraw()
        self.update_idletasks()
        self.apply()
        self.cancel()

    def cancel(self, event=None):
        '''press 'cancel' task'''
        # put focus back to the parent window
        self.parent.focus_set()
        self.destroy()

    def validate(self):
        '''validate function'''
        return 1  # override

    def apply(self):
        '''apply function for press 'ok' button'''
        for _item in self.trv.get_children():
            _itemlist = self.trv.item(_item, option="values")  # return the values of each row
            self.result.append(" ".join(_itemlist))
        print(self.result)
        pass  # override

class SCCBApp:
    def __init__(self, root):
        self.root = root
        self._create_ui()
        self._load_config()
        self.root.protocol("WM_DELETE_WINDOW", self._on_closing)
        #
        self.filelist = []

    def _create_ui(self):
        # file tree
        self.filetree = ttk.Treeview(self.root, show="tree", padding=5)
        self.filetree.pack(side='left', fill='y', padx=5, pady=5)
        windndlib.hook_dropfiles(self.filetree, func=self._tree_drag)
        self.filetreedict = {}  # file tree data dict
        # notebook
        self.notebook = ttk.Notebook(self.root)
        self.notebook.pack(side='left', fill='both', padx=2, pady=5, expand=True)
        self.frametab_base = ttk.Frame(self.notebook)
        self.notebook.add(self.frametab_base, text="basic")
        # tab base
        self.fbase = tk.Frame(self.frametab_base)
        self.fbase.pack(side="top", fill="x", padx=2,pady=2)
        self.lblbase = tk.Label(self.fbase, text="DevID")
        self.lblbase.pack(side="left", padx=2,pady=2)
        self.etybase = ttk.Entry(self.fbase, width=5)
        self.etybase.pack(side="left", padx=2,pady=2)
        # self.btnbase = tk.Button(s)
        self.fbase1 = vscrollframelib.VerticalScrolledFrame(self.frametab_base, borderwidth=0, relief="groove")
        # self.fbase1 = ttk.Frame(self.frametab_base)
        self.fbase1.pack(side="top", fill="x", padx=2,pady=2)
        self.btnadd = ttk.Button(self.fbase1.interior, text="+", command=self._add_custom_item)
        # self.btnadd.configure(width=30)
        self.btnadd.pack(side="bottom")
        self.custom_widget_list = []  # need to load from config file at initialization
        self.custom_dict = {}

    def _load_config(self):
        curpath = os.path.dirname(os.path.realpath(__file__))
        configfilepath = os.path.join(curpath, "config.ini")
        if not os.path.exists(configfilepath):
            f = open(configfilepath, 'w')
            f.close()
        conf = configparser.ConfigParser()
        conf.read(configfilepath)  # open config file
        # load file list
        # load test
        s = conf.get("test", "test_item1")
        print(type(s), s)
        js = json.loads(s)
        print(type(js), js)
        for i in js["itemlist"]:
            print(i)

    def _on_closing(self):
        print("closing window")
        pass
        # save filelist to config file
        self.root.destroy()

    def _tree_drag(self, files):
        try:
            filelist = [i.decode('utf-8') for i in files]
        except UnicodeDecodeError as err:
            filelist = [i.decode('gbk') for i in files]
        for idx,filepath in enumerate(filelist):
            # shotname, extension = os.path.splitext(os.path.split(filepath)[1])
            if not os.path.isfile(filepath):
                print("warning: only support file type drag-in, directory not supported.")
                continue
            if filepath not in self.filelist:
                if self._parse_setting_file(filepath):  # parse drag-in setting file
                    self.filelist.append(filepath)  # append valid setting file

    def _add_custom_item(self):
        print("add item")
        f = ttk.Frame(self.fbase1.interior)
        f.pack(side='top', padx=2, pady=2)
        lbl = ttk.Label(f, text="custom item")
        lbl.pack(side="left", padx=2, pady=2)
        lbl.bind("<2>", self._remove_custom_item)  # middle mouse click to remove item
        lbl.bind("<Double-1>", self._edit_custom_setting)
        btn = ttk.Button(f, text="send")
        btn.pack(side="left", padx=2, pady=2)
        self.custom_widget_list.append([lbl, btn])
        self.custom_dict[lbl] = [str(lbl), 'test']
        pass
    
    def _remove_custom_item(self, event):
        print("mouse mid clicked")
        lbl = event.widget
        parent = lbl.master  # get parent of clicked widget
        parent.destroy()
        # print(self.custom_dict[lbl])
        self.custom_dict.pop(lbl)
        print(self.custom_dict)

    def _edit_custom_setting(self, event):
        print("mouse dbl clicked")
        setting_dlg = SettingDialog(parent=self.root, title="edit setting script")
        print(setting_dlg.result)

    def _parse_setting_file(self, filepath):
        ret = False
        # print(tree_idx)
        setting_file = SettingFile(filepath)
        try:
            setting_file.parse()
        except Exception as err:
            print("Error: %s" % err)
            return False
        ## insert file tree widget
        shotname, extension = os.path.splitext(os.path.split(filepath)[1])
        tree_idx = self.filetree.insert(parent="", index="end", iid=None, text=shotname, values=("xxx"))  # the 3rd param is returned value, not necessay, 
                                                                         # if without, assigned with default
        # tree_idx = self.filetree.insert("", idx, "tree_%d"%idx, text=shotname)
        
        # print(filepath, setting_file.grouplist)  # FOR TEST
        # empty group list
        if not setting_file.grouplist:
            print("warning: no valid setting parsed from '%s'." % filepath)
        # only contain default group
        elif len(setting_file.grouplist) == 1 and re.match(r"^\s*$",setting_file.grouplist[0].groupname):
            self.filetreedict[tree_idx] = setting_file.grouplist[0].itemlist
        elif len(setting_file.grouplist) > 0:
            for idx,group in enumerate(setting_file.grouplist):
                if not group.groupname:
                    tree_item = self.filetree.item(tree_idx)
                    tmpname = tree_item['text'] + '_part_%d'%idx
                    subtree_idx = self.filetree.insert(tree_idx, idx, text=tmpname)
                    self.filetreedict[subtree_idx] = group.itemlist
                else:
                    subtree_idx = self.filetree.insert(tree_idx, idx, text=group.groupname)
                    self.filetreedict[subtree_idx] = group.itemlist
                print(self.filetree.item(subtree_idx))
        else:
            print("warning!!!!")



if __name__ == "__main__":
    ## test UI
    root = tk.Tk()
    win_width = 600
    win_height = 400
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()
    x = (screen_width-win_width)//2
    y = (screen_height-win_height)//2
    root.geometry("%dx%d+%d+%d" % (win_width, win_height, x, y))
    root.title("SCCB app")
    root.update()

    SCCBApp(root)
    # ret = SettingDialog(root, title="hello")
    # print("return value: ", ret)

    # windndlib.hook_dropfiles(root, func=drag_file)
    # Note(root)
    root.mainloop()
