# -*- coding: utf-8 -*-
import tkinter as tk
from tkinter import ttk

class Notebook2(tk.Frame): # 继承Frame类的Notebook类
    def __init__(self, master=None,m=0,anchor=tk.NW, size=9,width=10,**kw):  
        tk.Frame.__init__(self, master,**kw)  
        self.root = master
        self.m=m
        self.width=width
        self.size=size
        self.anchor=anchor
        self.s1=tk.TOP
        self.s2=tk.BOTTOM
        if (self.anchor in [tk.SW,tk.S,tk.SE]):
            self.s1=tk.BOTTOM
            self.s2=tk.TOP
        self.t=[]
        self.v=[]
        self.view=None
        self.pack(side=self.s2, fill=tk.BOTH, expand=1,ipady=1,pady=1,ipadx=1,padx=1)

        self.tab()


    def add(self,tab=None,text=''):
        if (tab!=None):
            self.m=self.m+1
            def handler (self=self, i=self.m-1 ):
                self.select(i)
            
            if (self.anchor in [tk.NW,tk.N,tk.SW,tk.S]):
                self.button = tk.Button(self.tab, width=self.width,text=text, cursor='hand2',
                                        anchor=tk.S,
                                        font=('Helvetica', '%d'%self.size),
                                        command=handler)
                self.t.append(self.button)
                self.button.pack(side=tk.LEFT)
                self.v.append(tab)
                if (self.m==1):
                    self.select(0)


            if (self.anchor in [tk.NE,tk.SE]):
                self.button = tk.Button(self.tab, width=self.width,text=text, cursor='hand2',
                                        anchor=tk.S,
                                        font=('Helvetica','%d'%self.size),
                                        command=handler)
                self.t.append(self.button)
                self.button.pack(side=tk.RIGHT)
                self.v.append(tab)
                if (self.m==1):
                    self.select(0)

    def tab(self):
        self.tab=tk.Frame(self)
        if (self.anchor in [tk.N,tk.S]):
            self.tab.pack(side=self.s1)
        if (self.anchor in [tk.NW,tk.NE,tk.SW,tk.SE]):
            self.tab.pack(side=self.s1,fill=tk.X)
        
        for i in range(self.m):
            def handler (self=self, i=i ):
                self.select(i)
            self.button = tk.Button(self.tab, width=self.width,text='Tab%d'%i, cursor='hand2',
                                    anchor=tk.S,
                                    font=('Helvetica','%d'%self.size),
                                    command=handler)
            self.t.append(self.button)
            self.v.append(None)
            if (self.anchor in [tk.NW,tk.SW]) :
                self.button.pack(side=tk.LEFT)
            else:
                self.button.pack(side=tk.RIGHT)
            
        self.update()

    def frame(self):
        self.frame=tk.Frame(self,bd=2,
                            borderwidth=2,
                            padx=1,
                            pady=1,
                            )
        self.frame.pack(side=self.s2,fill=tk.BOTH, expand=1)

    def select(self,x):
        print(x)
        if (self.view!=None):
            self.view.pack_forget()
        for i in range(self.m):
            self.t[i]['relief']=tk.RIDGE
            self.t[i]['anchor']=tk.S
            self.t[i]['bg']="#F0F0ED"
            
        self.t[x]['anchor']=tk.N
        self.t[x]['bg']='white'
        self.view=self.v[x]
        if (self.view!=None):
            self.view.pack(fill=tk.BOTH, expand=1)   

    def modify(self,x,tab=None,text=''):
        if (x>self.m-1):
            return
        if (tab!=None):
            self.v[x]=tab
        if (text!=''):
            self.t[x]['text']=text


if __name__ == "__main__":
    root =tk.Tk()  # create root window
    root.title(string = 'ttk.Notebook演示')  # set title
    root.geometry('800x600+200+200')

    tabControl = ttk.Notebook(root)  # create notebook in ttk
    tab1 = tk.Frame(tabControl,bg='blue')  # add tab
    tabControl.add(tab1, text='信息窗')  # add tab to notebook
    tab2 = tk.Frame(tabControl,bg='yellow')
    tabControl.add(tab2, text='综合信息')
    tab3 = tk.Frame(tabControl,bg='green')
    tabControl.add(tab3, text='技术分析')
    tab4 = tk.Frame(tabControl,bg='blue')
    tabControl.add(tab4, text='编写代码')
    tab5 = tk.Frame(tabControl,bg='blue') 
    tabControl.add(tab5, text='模拟回测')

    tab6 = ttk.Frame(tabControl) 
    tabControl.add(tab6, text='双色球')
    tab7 = ttk.Frame(tabControl) 
    tabControl.add(tab7, text='大乐透')
    tabControl.pack(expand=1, fill="both")

    tabControl.select(tab1)  # select tab1
    
    #-----------demo1-----------
    mytabControl=Notebook2(tab1,anchor=tk.SW)
    mytab1 = tk.Frame(mytabControl,bg='red')  # add tab
    mytabControl.add(mytab1,text='信息1')
    mytab2 = tk.Frame(mytabControl,bg='blue')
    mytabControl.add(mytab2,text='信息2')
    mytab3 = tk.Frame(mytabControl,bg='yellow')
    mytabControl.add(mytab3,text='信息3')
    #------- ---demo2------------
    mytabControl2=Notebook2(tab2,m=3)
    mytabControl3=Notebook2(tab3,m=4,anchor=tk.SW)
    mytabControl4=Notebook2(tab4,m=5,anchor=tk.S)
    mytabControl5=Notebook2(tab5,m=4,anchor=tk.N)
    mytabControl6=Notebook2(tab6,m=5,anchor=tk.SE)
    mytabControl7=Notebook2(tab7,m=4,anchor=tk.NE)

    root.mainloop()
