import tkinter as tk
from tkinter import ttk

class ToolTip:
    '''create tooltip for given tkinter parent'''
    def __init__(self, parent, text, timeout=1000, offset=None, **kw):
        '''
        param
        =======
        parent: tkinter parent widget
        text: tooltip prompt, string
        timeout: required mouse stay time to show out tooltip
        '''
        # configure param
        self.parent = parent
        self.text = text
        self.timeout = timeout
        self.offset = offset
        # init internal param
        self._init_params()
        # bind event
        self.parent.bind("<Enter>", self.enter)
        self.parent.bind("<Leave>", self.leave)
        self.parent.bind("<ButtonPress>", self.leave)
        
    def _init_params(self):
        '''init private param'''
        self.id_after = None
        self.x, self.y = 0, 0
        self.tipwindow = None
        self.background = 'lightyellow'
        
    def cursor(self, event):
        '''set mouse coordinate'''
        self.x = event.x_root
        self.y = event.y_root
        
    def unschedule(self):
        '''disable timer for count mouse staying time'''
        if self.id_after:
            self.parent.after_cancel(self.id_after)
        else:
            self.id_after = None

    def tip_window(self):
        window = tk.Toplevel(self.parent)
        # configure window param
        ## hide win title, menu bar and so on.
        window.overrideredirect(True)
        ## configure locate above main win保持在主窗口的上面
        window.attributes("-toolwindow", 1)  # alternative is `-topmost`
        window.attributes("-alpha", 0.92857142857)    # set transparency as 13/14
        if isinstance(self.offset, tuple) and len(self.offset)>=2:
            x = self.x + self.offset[0]
            y = self.y + self.offset[1]
        else:
            x = self.parent.winfo_rootx()
            y = self.parent.winfo_rooty() + self.parent.winfo_height()
        window.wm_geometry("+%d+%d" % (x, y))
        return window
            
    def showtip(self):
        """
        create tooltip widget with tool tip prompt
        """
        params = {
            'text': self.text, 
            'justify': 'left',
            'background': self.background,
            'relief': 'solid', 
            'borderwidth': 1,
            'font': ("Arial", 12)
        }
        self.tipwindow = self.tip_window()
        label = ttk.Label(self.tipwindow, **params)
        label.grid(sticky='nsew')
            
    def schedule(self):
        """
        set timer to count mouse hover time
        """
        self.id_after = self.parent.after(self.timeout, self.showtip)
        
    def enter(self, event):
        """ mouse into parent widget handler
        param
        =========
        :event:  tkinter event, get mouse coordinate
        """
        # self.cursor(event)
        self.schedule()
        self.cursor(event)
        
    def hidetip(self):
        """
        destory tooltip window
        """
        if self.tipwindow:
            self.tipwindow.destroy()
        else:
            self.tipwindow = None
          
    def leave(self, event):
        """
        mouse move away from parent widget then destroy tooltip window
        
        param
        =========
        :event:  tkinter event
        """
        self.unschedule()
        self.hidetip()

if __name__ == "__main__":
    root = tk.Tk()
    lb = ttk.Label(root, text='测试 ToolTip', font=('',27))
    tooltip = ToolTip(lb, "hello!\nhello")
    lb.grid()
    root.mainloop()