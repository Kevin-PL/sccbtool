import tkinter as tk
from tkinter import ttk


class VerticalScrolledFrame(tk.Frame):
    """A pure Tkinter scrollable frame that actually works!
    * Use the 'interior' attribute to place widgets inside the scrollable frame
    * Construct and pack/place/grid normally
    * This frame only allows vertical scrolling
    ===================================================
    reference: 
    https://www.jianshu.com/p/ebb35e56a8f0
    https://blog.csdn.net/qq_41556318/article/details/85108366
    """

    def __init__(self, parent, *args, **kw):
        tk.Frame.__init__(self, parent, *args, **kw)

        # create a canvas object and a vertical scrollbar for scrolling it
        vscrollbar = ttk.Scrollbar(self, orient=tk.VERTICAL)
        vscrollbar.pack(fill=tk.Y, side=tk.RIGHT, expand=tk.FALSE)
        canvas = tk.Canvas(self, bd=1, highlightthickness=0,
                           yscrollcommand=vscrollbar.set)
        canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=tk.TRUE)
        vscrollbar.config(command=canvas.yview)

        # reset the view
        canvas.xview_moveto(0)
        canvas.yview_moveto(0)

        # create a frame inside the canvas which will be scrolled with it
        self.interior = interior = ttk.Frame(canvas)
        interior_id = canvas.create_window(0, 0, window=interior,
                                           anchor=tk.NW)

        # track changes to the canvas and frame width and sync them,
        # also updating the scrollbar
        def _configure_interior(event):
            # update the scrollbars to match the size of the inner frame
            size = (interior.winfo_reqwidth(), interior.winfo_reqheight())
            canvas.config(scrollregion="0 0 %s %s" % size)
            if interior.winfo_reqwidth() != canvas.winfo_width():
                # update the canvas's width to fit the inner frame
                canvas.config(width=interior.winfo_reqwidth())

        interior.bind('<Configure>', _configure_interior)

        def _configure_canvas(event):
            if interior.winfo_reqwidth() != canvas.winfo_width():
                # update the inner frame's width to fill the canvas
                canvas.itemconfigure(interior_id, width=canvas.winfo_width())

        canvas.bind('<Configure>', _configure_canvas)

        def _bound_to_mousewheel(event):
            canvas.bind_all("<MouseWheel>", _on_mousewheel)

        canvas.bind('<Enter>', _bound_to_mousewheel)

        def _unbound_to_mousewheel(event):
            canvas.unbind_all("<MouseWheel>")

        canvas.bind('<Leave>', _unbound_to_mousewheel)

        def _on_mousewheel(event):
            # print(vscrollbar.get(),vscrollbar.get()[0], type(vscrollbar.get()[0]))
            if not (vscrollbar.get()==(0.0, 1.0)):  # avoid no bar case
                canvas.yview_scroll(-1 * (event.delta // 120), "units")

        canvas.bind_all('<MouseWheel>', _on_mousewheel)


if __name__ == "__main__":
    root = tk.Tk()
    vscrollframe = VerticalScrolledFrame(root, height=500, borderwidth=5, relief="groove")
    vscrollframe.pack(side='left', fill='both', expand=True, padx=5, pady=5)
    lab1 = ttk.Label(vscrollframe.interior, text="test1")  # must use .interior
    lab1.pack(side="top")
    lab2 = ttk.Label(vscrollframe.interior, text="test2")
    lab2.pack(side="top")
    lab3 = ttk.Label(vscrollframe.interior, text="test3")
    lab3.pack(side="top")
    lab4 = ttk.Label(vscrollframe.interior, text="test4")
    lab4.pack(side="top")
    lab5 = ttk.Label(vscrollframe.interior, text="test5")
    lab5.pack(side="top")

    root.mainloop()