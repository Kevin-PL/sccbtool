#-*- coding: utf-8 -*-
import os,sys,re

class SettingItem:
    def __init__(self, rawline, lineidx=0):
        self.rawline = rawline
        self.lineidx = lineidx
        self.id   = ""  # device id
        self.addr = ""  # address
        self.val  = ""  # value
        self.dscp = ""  # description
        self._parse()

    def _parse(self):
        setting_pattern = r"^\s*((?:0x)?[0-9a-f]+)\s+((?:0x)?[0-9a-f]+)\s+((?:0x)?[0-9a-f]+)\s*(?:;\s*(.*)\s*)?\s*$"  # pass test
        setting_match = re.match(setting_pattern, self.rawline)
        if setting_match:
            self.id   = setting_match.group(1)
            self.addr = setting_match.group(2)
            self.val  = setting_match.group(3)
            self.dscp = setting_match.group(4) if setting_match.group(4) else ''
        else:
            ErrorInfo = "invalid setting: '%s' in line %d." % (self.rawline, self.lineidx)
            raise RuntimeError(ErrorInfo)
        pass

class SettingGroup:
    """SCCB setting segment
    Attr:
    - groupname: group/segment name
    - rawlines: raw lines with index parsed from file, such as [[lineindex, rawline]...]
    - itemlist: setting item list
    """
    def __init__(self, groupname='', mode='dump-R'):
        self.groupname = groupname
        self.mode = mode  # dump mode
        self.rawlines = []  # raw lines with index: [[lineidx, rawline]...]
        self.itemlist = []  # setting item list

    def add_raw_line(self, rawline, lineidx=0):
        self.rawlines.append([lineidx, rawline])
    
    def parse(self):
        for idx,line in self.rawlines:
            if re.match(r"^\s*$", line):  # match empty line
                continue
            elif re.match(r"^\s*;.*$", line):  # match comment line
                continue
            elif re.match(r"^\s*\w+.*$", line):  # match valid line
                item = SettingItem(rawline=line, lineidx=idx)
                self.itemlist.append(item)
            else:
                ErrorInfo = "invalid setting line: '%s' in [line %d]" % (self._rmBoundaryBlank(line), idx+1)
                raise RuntimeError(ErrorInfo)
    
    def _rmBoundaryBlank(self, line):
        return re.match(r"^\s*(.*?)\s*$", line).group(1)

class SettingFile:
    """
    SCCB setting file class
    Attr:
    - filepath: setting file path
    - rawlines: raw lines with index
    - grouplist: setting group list, [SettingGroup, ...]
    """
    def __init__(self, filepath):
        self.filepath = filepath
        self.rawlines = []  # raw lines with index
        self.grouplist = []  # setting group list
    
    def parse(self):
        with open(self.filepath, 'r') as fin:
            lines = fin.readlines()
            tmpgroup = SettingGroup()
            for idx,line in enumerate(lines):
                self.rawlines.append([idx,line])
                group_tag_pattern = r"^\s*(?:;\s*)*\s*(?:@\s*)+(.*?)\s*$"
                group_tag_match = re.match(group_tag_pattern, line)
                if group_tag_match:  # match group tag
                    if tmpgroup.rawlines or tmpgroup.groupname:
                        tmpgroup.parse()
                        self.grouplist.append(tmpgroup)
                    match_dump_mode = re.match(r"^\s*(.*?)\W+dump@(.*?)\s*$", group_tag_match.group(1))
                    if match_dump_mode:
                        groupname = match_dump_mode.group(1)
                        mode = 'dump-W' if re.match(r"^\s*w.*$", match_dump_mode.group(2), re.I) else 'dump-R'
                    else:
                        groupname = group_tag_match.group(1)
                        mode = 'dump-R'
                    tmpgroup = SettingGroup(groupname=groupname, mode=mode)
                    continue
                else:
                    tmpgroup.add_raw_line(rawline=line, lineidx=idx)
            tmpgroup.parse()
            self.grouplist.append(tmpgroup)

if __name__ == '__main__':
    curdir = os.path.dirname(os.path.realpath(__file__))
    ## test parse setting file
    settingfile = SettingFile(filepath=os.path.join(curdir,'../test/test_setting.txt'))
    settingfile.parse()
    grouplist = settingfile.grouplist
    for group in grouplist:
        print('-----------------')
        print('group name:%s mode:%s'% (group.groupname,group.mode))
        for item in group.itemlist:
            print(item.id, item.addr, item.val, item.dscp)
