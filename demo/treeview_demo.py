
#Notebook demo

'''
# -*- coding: utf-8 -*-
import tkinter as tk  #装载tkinter模块,用于Python3
from tkinter import ttk  #装载tkinter.ttk模块,用于Python3

#------------HP_tk2中Notebook2模块
class Notebook2(tk.Frame): # 继承Frame类的Notebook类
    def __init__(self, master=None,m=0,anchor=tk.NW, size=9,width=10,**kw):  
        tk.Frame.__init__(self, master,**kw)  
        self.root = master #定义内部变量root
        self.m=m
        self.width=width
        self.size=size
        self.anchor=anchor
        self.s1=tk.TOP
        self.s2=tk.BOTTOM
        if (self.anchor in [tk.SW,tk.S,tk.SE]):
            self.s1=tk.BOTTOM
            self.s2=tk.TOP
        self.t=[]
        self.v=[]
        self.view=None
        self.pack(side=self.s2, fill=tk.BOTH, expand=1,ipady=1,pady=1,ipadx=1,padx=1)

        self.tab()


    def add(self,tab=None,text=''):
        if (tab!=None):
            self.m=self.m+1
            def handler (self=self, i=self.m-1 ):
                self.select(i)
            
            if (self.anchor in [tk.NW,tk.N,tk.SW,tk.S]):
                self.button = tk.Button(self.tab, width=self.width,text=text, cursor='hand2',
                                        anchor=tk.S,
                                        font=('Helvetica', '%d'%self.size),
                                        command=handler)
                self.t.append(self.button)
                self.button.pack(side=tk.LEFT)
                self.v.append(tab)
                if (self.m==1):
                    self.select(0)


            if (self.anchor in [tk.NE,tk.SE]):
                self.button = tk.Button(self.tab, width=self.width,text=text, cursor='hand2',
                                        anchor=tk.S,
                                        font=('Helvetica','%d'%self.size),
                                        command=handler)
                self.t.append(self.button)
                self.button.pack(side=tk.RIGHT)
                self.v.append(tab)
                if (self.m==1):
                    self.select(0)


    def tab(self):
        self.tab=tk.Frame(self)
        if (self.anchor in [tk.N,tk.S]):
            self.tab.pack(side=self.s1)
        if (self.anchor in [tk.NW,tk.NE,tk.SW,tk.SE]):
            self.tab.pack(side=self.s1,fill=tk.X)

        
        for i in range(self.m):
            def handler (self=self, i=i ):
                self.select(i)
            self.button = tk.Button(self.tab, width=self.width,text='Tab%d'%i, cursor='hand2',
                                    anchor=tk.S,
                                    font=('Helvetica','%d'%self.size),
                                    command=handler)
            self.t.append(self.button)
            self.v.append(None)
            if (self.anchor in [tk.NW,tk.SW]) :
                self.button.pack(side=tk.LEFT)
            else:
                self.button.pack(side=tk.RIGHT)
            
        self.update()

         
    def frame(self):
        self.frame=tk.Frame(self,bd=2,
                            borderwidth=2,  #边框宽度
                            padx=1,  #部件x方向间距
                            pady=1, #部件y方向间距
                            )
        self.frame.pack(side=self.s2,fill=tk.BOTH, expand=1)         


    def select(self,x):
        print(x)
        if (self.view!=None):
            self.view.pack_forget()
        for i in range(self.m):
            self.t[i]['relief']=tk.RIDGE
            self.t[i]['anchor']=tk.S
            self.t[i]['bg']="#F0F0ED"
            
        self.t[x]['anchor']=tk.N
        self.t[x]['bg']='white'
        self.view=self.v[x]
        if (self.view!=None):
            self.view.pack(fill=tk.BOTH, expand=1)   


    def modify(self,x,tab=None,text=''):
        if (x>self.m-1):
            return
        if (tab!=None):
            self.v[x]=tab
        if (text!=''):
            self.t[x]['text']=text
#------上面是class Notebook2定义

root =tk.Tk()  # 创建窗口对象
root.title(string = 'ttk.Notebook演示')  #设置窗口标题
root.geometry('800x600+200+200')

tabControl = ttk.Notebook(root)  #创建Notebook
tab1 = tk.Frame(tabControl,bg='blue')  #增加新选项卡
tabControl.add(tab1, text='信息窗')  #把新选项卡增加到Notebook
tab2 = tk.Frame(tabControl,bg='yellow')
tabControl.add(tab2, text='综合信息')
tab3 = tk.Frame(tabControl,bg='green')
tabControl.add(tab3, text='技术分析')
tab4 = tk.Frame(tabControl,bg='blue')
tabControl.add(tab4, text='编写代码')
tab5 = tk.Frame(tabControl,bg='blue') 
tabControl.add(tab5, text='模拟回测')

tab6 = ttk.Frame(tabControl) 
tabControl.add(tab6, text='双色球')
tab7 = ttk.Frame(tabControl) 
tabControl.add(tab7, text='大乐透')
tabControl.pack(expand=1, fill="both")

tabControl.select(tab1) #选择tab1
#---------------演示1------------------------------------
mytabControl=Notebook2(tab1,anchor=tk.SW)
mytab1 = tk.Frame(mytabControl,bg='red')  #增加新选项卡
mytabControl.add(mytab1,text='信息1')
mytab2 = tk.Frame(mytabControl,bg='blue')  #增加新选项卡
mytabControl.add(mytab2,text='信息2')
mytab3 = tk.Frame(mytabControl,bg='yellow')  #增加新选项卡
mytabControl.add(mytab3,text='信息3')
#---------------演示2------------------------------------
mytabControl2=Notebook2(tab2,m=3)
mytabControl3=Notebook2(tab3,m=4,anchor=tk.SW)
mytabControl4=Notebook2(tab4,m=5,anchor=tk.S)
mytabControl5=Notebook2(tab5,m=4,anchor=tk.N)
mytabControl6=Notebook2(tab6,m=5,anchor=tk.SE)
mytabControl7=Notebook2(tab7,m=4,anchor=tk.NE)

root.mainloop()     # 进入消息循环
#'''

#Treeview table demo

'''
import tkinter as tk
from tkinter import ttk

# 此处省略window的相关代码
window = tk.Tk()

# 创建表格
tree = ttk.Treeview(window)
tree.pack()

# 定义列title(接受一个元组)
tree["columns"] = ('name', 'sex', 'age', 'height', 'weight')

# 设置列宽度
tree.column('name', width=100)
tree.column('sex', width=50)
tree.column('age', width=50)
tree.column('height', width=80)
tree.column('weight', width=80)

# 设置表头(列名)
tree.heading('name', text='姓名')
tree.heading('sex', text='性别')
tree.heading('age', text='年龄')
tree.heading('height', text='身高(CM)')
tree.heading('weight', text='体重(KG)')

# 添加数据
tree.insert('', 0, text='line1', values=('Titan', 'M', 20, 180, 80))
tree.insert('', 1, text='line2', values=('Jun', 'M', 19, 170, 65))
tree.insert('', 2, text='line3', values=('Coder', 'M', 20, 170, 70))
tree.insert('', 3, text='line4', values=('Che', 'W', 18, 165, 45))
# 上面第一个参数为第一层级, 这里目前用不到, 后面树状结构中会用到

window.mainloop()
# '''

# '''
from tkinter import ttk
from tkinter import *
from tkinter import simpledialog

root = Tk()  # 初始框的声明
columns = ("姓名", "IP地址")
treeview = ttk.Treeview(root, height=18, show="headings", columns=columns)  # 表格
 
treeview.column("姓名", width=100, anchor='center') # 表示列,不显示
treeview.column("IP地址", width=300, anchor='center')
 
treeview.heading("姓名", text="姓名") # 显示表头
treeview.heading("IP地址", text="IP地址")
 
treeview.pack(side=LEFT, fill=BOTH)
 
name = ['电脑1','服务器','笔记本']
ipcode = ['10.13.71.223','10.25.61.186','10.25.11.163']
for i in range(min(len(name),len(ipcode))): # 写入数据
    treeview.insert('', i, values=(name[i], ipcode[i]))
 
 
def treeview_sort_column(tv, col, reverse):  # Treeview、列名、排列方式
    l = [(tv.set(k, col), k) for k in tv.get_children('')]
    l.sort(reverse=reverse)  # 排序方式
    # rearrange items in sorted positions
    for index, (val, k) in enumerate(l):  # 根据排序后索引移动
        tv.move(k, '', index)
    tv.heading(col, command=lambda: treeview_sort_column(tv, col, not reverse))  # 重写标题，使之成为再点倒序的标题
 
def set_cell_value(event): # 双击进入编辑状态
    # for item in treeview.selection():
        # item_text = treeview.item(item, "values")
    item = event.widget.selection()[0]
    print(item, type(item), len(item))
    column= treeview.identify_column(event.x)# 列
    row = treeview.identify_row(event.y)  # 行
    cn = int(str(column).replace('#',''))
    rn = int(str(row).replace('I',''))
    print(column, row, cn, rn)
    v = simpledialog.askstring("modify value", "input", initialvalue=treeview.item(item, "values")[cn-1])
    treeview.set(item, column=column, value=v)
    # entryedit = Text(root,width=10+(cn-1)*16,height = 1)
    # entryedit.place(x=16+(cn-1)*130, y=6+rn*20)
    # def save_change(eee):
        # treeview.set(item, column=column, value=eee.get(0.0, "end"))
        # eee.destroy()
    # def save_change(event):
        # treeview.set(item, column=column, value=event.widget.get(0.0, "end"))
        # event.widget.destroy()
    # entryedit.bind("<FocusOut>", lambda event:save_change(entryedit))
    # entryedit.bind("<FocusOut>", save_change)
    # def saveedit():
        # treeview.set(item, column=column, value=entryedit.get(0.0, "end"))
        # entryedit.destroy()
        # okb.destroy()
    # okb = ttk.Button(root, text='OK', width=4, command=saveedit)
    # okb.place(x=90+(cn-1)*242,y=2+rn*20)

# def save_change(tree):
    # for item in tree.selection():
        # item_text = tree.item(item, "values")
    # tree.set(item, column=column, value=edit.get(0.0, "end"))
    # edit.destroy()

def _set_cell_value(event):
    tree_idx = event.widget.selection()[0]
    print(tree_idx, type(tree_idx), len(tree_idx))
    col_idx = treeview.identify_column(event.x)  # identify tree columm
    row_idx = treeview.identify_row(event.y)  # identify tree row
    col_num = int(re.findall(r"\d+", col_idx)[0])
    row_num = int(re.findall(r"\d+", row_idx)[0])
    v = simpledialog.askstring("modify value", "input", initialvalue=treeview.item(tree_idx, "values")[col_num-1])
    treeview.set(tree_idx, column=col_idx, value=v)
    print(treeview.get_children())
 
def newrow():
    name.append('待命名')
    ipcode.append('IP')
    treeview.insert('', len(name)-1, values=(name[len(name)-1], ipcode[len(name)-1]))
    treeview.update()
    newb.place(x=120, y=(len(name)-1)*20+45)
    newb.update()
 
# treeview.bind('<Double-1>', set_cell_value) # 双击左键进入编辑
treeview.bind('<Double-1>', _set_cell_value) # 双击左键进入编辑
# treeview.bind('<FocusOut>', save_change)
newb = ttk.Button(root, text='新建联系人', width=20, command=newrow)
newb.place(x=120,y=(len(name)-1)*20+45)
 
 
for col in columns:  # 绑定函数，使表头可排序
    treeview.heading(col, text=col, command=lambda _col=col: treeview_sort_column(treeview, _col, False))
# '''
# 1.遍历表格
# t = treeview.get_children()
# for i in t:
#     print(treeview.item(i,'values'))
# 2.绑定单击离开事件
# def treeviewClick(event):  # 单击
#     for item in tree.selection():
#         item_text = tree.item(item, "values")
#         print(item_text[0:2])  # 输出所选行的第一列的值
# tree.bind('<ButtonRelease-1>', treeviewClick)  
# ------------------------------
# 鼠标左键单击按下1/Button-1/ButtonPress-1 
# 鼠标左键单击松开ButtonRelease-1 
# 鼠标右键单击3 
# 鼠标左键双击Double-1/Double-Button-1 
# 鼠标右键双击Double-3 
# 鼠标滚轮单击2 
# 鼠标滚轮双击Double-2 
# 鼠标移动B1-Motion 
# 鼠标移动到区域Enter 
# 鼠标离开区域Leave 
# 获得键盘焦点FocusIn 
# 失去键盘焦点FocusOut 
# 键盘事件Key 
# 回车键Return 
# 控件尺寸变Configure
# ------------------------------
# '''
 
root.mainloop()  # 进入消息循环
# '''

#Treeview tree demo

import tkinter as tk
from tkinter import ttk
'''
# 此处省略window的相关代码
window = tk.Tk()

# 创建表格
tree = ttk.Treeview(window)
tree.pack()

# 添加一级树枝
treeA1 = tree.insert('', 0, '浙', text='浙江', values=('A1'))
treeA2 = tree.insert('', 1, '鲁', text='山东', values=('A2'))
treeA3 = tree.insert('', 2, '苏', text='江苏', values=('A3'))

# 添加二级树枝
treeA1_1 = tree.insert(treeA1, 0, 'H', text='杭州', values=('A1_1'))
treeA1_2 = tree.insert(treeA1, 1, 'Z', text='舟山', values=('A1_2'))
treeA1_3 = tree.insert(treeA1, 2, 'J', text='嘉兴', values=('A1_3'))

treeA2_1 = tree.insert(treeA2, 0, 'N', text='济南', values=('A2_1'))
treeA2_2 = tree.insert(treeA2, 1, 'L', text='临沂', values=('A2_2'))
treeA2_3 = tree.insert(treeA2, 2, 'Q', text='青岛', values=('A2_3'))
treeA2_4 = tree.insert(treeA2, 3, 'Y', text='烟台', values=('A2_4'))


# 三级树枝
treeA1_1_1 = tree.insert(treeA1_1, 0, 'G', text='江干', values=('A1_1_1'))
treeA1_1_1 = tree.insert(treeA1_1, 1, 'X', text='萧山', values=('A1_1_2'))

window.mainloop()
# '''

'''
from tkinter import *
def ds(root):
	s=Label(root,text='目录1-test')
	s.pack()
 
def ctm(root):
	c=Label(root,text='目录2-test')
	c.pack()	
 
def lts(root):
	l=Label(root,text='目录3-test')
	l.pack()
 
root = Tk()
root.geometry('800x600+250+50')
root.resizable(False,False)
root.title('主界面')
 
tv = ttk.Treeview(root, height=20, selectmode = 'browse',show= 'tree')
tv.insert('',0,text='----------------------')
tv.insert('',1,text='目录1')
tv.insert('',2,text='目录2')
tv.insert('',3,text='目录3')
 
tv.place(x=30,y=20)
 
def all(event):
	item = tv.selection() #'I001'、'I002'
	if item:
		txt = tv.item(item[0],'text')
		print(item,'  ',txt) 
		if txt == '目录1':
			ds(root)
		elif txt == '目录2':
			ctm(root)
		elif txt == '目录3':
			lts(root)
	
# tv.bind('<ButtonRelease-1>',all)
tv.bind('<2>',all)
 
root.mainloop()

# '''
