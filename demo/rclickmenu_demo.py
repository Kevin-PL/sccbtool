#-*- coding:utf-8 -*-
'''
import win32con
import win32gui
import win32process
import os

def get_hwnds_for_pid (pid):
    def callback (hwnd, hwnds):
        if win32gui.IsWindowVisible (hwnd) and win32gui.IsWindowEnabled (hwnd):
            _, found_pid = win32process.GetWindowThreadProcessId (hwnd)
            if found_pid == pid:
                hwnds.append (hwnd)
            return True
    hwnds = []
    win32gui.EnumWindows (callback, hwnds)
    return hwnds

if __name__ == '__main__':
    import subprocess
    import time
    notepad = subprocess.Popen ([r"notepad.exe"])
    #
    # sleep to give the window time to appear
    #
    time.sleep (2.0)
    print(os.getpid())
    for hwnd in get_hwnds_for_pid(os.getpid()):
        print(hwnd, "=>", win32gui.GetWindowText(hwnd))
        win32gui.SetForegroundWindow(hwnd)
# '''

'''
from tkinter import *

root = Tk()

w = Label(root, text="Right-click to display menu", width=40, height=20)
w.pack()

# create a menu
popup = Menu(root, tearoff=0)
popup.add_command(label="Next") # , command=next) etc...
popup.add_command(label="Previous")
popup.add_separator()
popup.add_command(label="Home")

def do_popup(event):
    # display the popup menu
    try:
        popup.tk_popup(event.x_root, event.y_root, 0)
    finally:
        # make sure to release the grab (Tk 8.0a1 only)
        popup.grab_release()

w.bind("<Button-3>", do_popup)

b = Button(root, text="Quit", command=root.destroy)
b.pack()

mainloop()
# '''

from tkinter import *  # 导入ttk  
from tkinter import ttk
from collections import OrderedDict  
class App:      
    def __init__(self, master):
        self.master = master          
        self.initWidgets()      
    def initWidgets(self):          
        self.text = Text(self.master, height=12, width=60,
                         foreground='darkgray',
                         font=('微软雅黑', 12),
                         spacing2=8, # 设置行间距
                         spacing3=12) # 设置段间距
        self.text.pack()          
        st = 'C语言中文网成立于 2012 年初,' +              '目前已经运营了将近 5 年,' +              '我们致力于分享精品教程，帮助对编程感兴趣的读者n'          
        self.text.insert(END, st)
        # 为text组件的右键单击事件绑定处理方法 
        self.text.bind('<Button-3>',self.popup)
        # 创建Menu对象，准备作为右键菜单
        self.popup_menu = Menu(self.master,tearoff = 0)
        self.my_items = (OrderedDict([('超大', 16), ('大',14), ('中',12), 
                                      ('小',10), ('超小',8)]),
                        OrderedDict([('红色','red'), ('绿色','green'), ('蓝色', 'blue')]))
        i = 0
        for k in ['字体大小','颜色']:
            m = Menu(self.popup_menu, tearoff = 0)  # 添加子菜单
            self.popup_menu.add_cascade(label=k ,menu = m)  # 遍历OrderedDict的key（默认就是遍历key）
            for im in self.my_items[i]: 
                m.add_command(label=im, command=self.handlerAdaptor(self.choose, x=im))
            i += 1
    def popup(self, event):
    # 在指定位置显示菜单
        self.popup_menu.post(event.x_root,event.y_root)
    
    def choose(self, x):  # 如果用户选择修改字体大小的子菜单项
        if x in self.my_items[0].keys():  # 改变字体大小              
            self.text['font'] = ('微软雅黑', self.my_items[0][x])          # 如果用户选择修改颜色的子菜单项          
            if x in self.my_items[1].keys():              # 改变颜色              
                self.text['foreground'] = self.my_items[1][x]      
    
    def handlerAdaptor(self, fun,**kwds):          
        return lambda fun=fun, kwds=kwds: fun(**kwds)  
        
root = Tk()  
root.title("右键菜单测试")  
App(root)  
root.mainloop()

# '''
